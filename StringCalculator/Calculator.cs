﻿using System;
using System.Collections.Generic;

namespace StringCalculator
{
    public class Calculator
    {
        public int Add(string numbers)
        {
            if (string.IsNullOrEmpty(numbers))
            {
                return 0;
            }

            string[] delimiters = GetDelimiters(numbers);
            List<int> numbersArray = GetNumbers(numbers, delimiters);

            ValidateNumbers(numbersArray);

            return GetSum(numbersArray);
        }

        private void ValidateNumbers(List<int> numbersArray)
        {
            List<string> negativeNumbers = new List<string>();

            foreach (var number in numbersArray)
            {
                if (number < 0)
                {
                    negativeNumbers.Add(number.ToString());
                }
            }

            if (negativeNumbers.Count != 0)
            {
                throw new Exception($"Negative numbers not allowed {string.Join(",", negativeNumbers)}");
            }
        }

        private int GetSum(List<int> numbersArray)
        {
            int sum = 0;

            foreach (var number in numbersArray)
            {
                if (number < 1001)
                {
                    sum += number;
                }
            }

            return sum;
        }

        private List<int> GetNumbers(string numbers, string[] delimiters)
        {
            string[] stringNumbers = numbers.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
            List<int> numbersArray = new List<int>();

            foreach (var num in stringNumbers)
            {
                if (int.TryParse(num, out int number))
                {
                    numbersArray.Add(number);
                }
            }

            return numbersArray;
        }

        private string[] GetDelimiters(string numbers)
        {
            var customDelimiterId = "//";
            var newline = "\n";
            var multipleCustomDelimitersId = $"{customDelimiterId}[";
            var multipleCustomDelimitersSeperator = $"]{newline}";

            if (numbers.StartsWith(multipleCustomDelimitersId))
            { 
                string delimiter = numbers.Substring(numbers.IndexOf(multipleCustomDelimitersId) + multipleCustomDelimitersId.Length, numbers.)
            }
            else if (numbers.StartsWith(customDelimiterId))
            {
                return new[] { numbers.Substring(numbers.IndexOf(customDelimiterId) + customDelimiterId.Length, numbers.IndexOf(newline) - (newline.Length + 1)) };
            }

            return new[] { ",", newline };
        }
    }
}
